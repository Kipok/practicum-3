#include <unistd.h>
#include <fcntl.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>

#define swap(a, b) (a) ^= (b), (b) ^= (a), (a) ^= (b)

int
main(int argc, char *argv[])
{
    int fd[2][2], n = 1, c = 0;
    if (pipe(fd[c]) < 0) {
        return 1;
    }
    if (!fork()) {
        if (argc > 2) {
            dup2(fd[c][1], 1);
        }
        close(fd[c][1]);
        close(fd[c][0]);
        execlp(argv[1], argv[1], NULL);
    }
    close(fd[c][1]);
    if (argc == 2) {
        close(fd[c][0]);
        wait(NULL);
        return 0;
    }
    for (int i = 2; i < argc; i++) {
        if (pipe(fd[n]) < 0) {
            return 1;
        }
        if (!fork()) {
            dup2(fd[c][0], 0);
            if (i != argc - 1) {
                dup2(fd[n][1], 1);
            }
            close(fd[c][0]);
            close(fd[c][1]);
            close(fd[n][1]);
            close(fd[n][0]);
            execlp(argv[i], argv[i], NULL);
        }
        close(fd[n][1]);
        close(fd[c][0]);
        swap(n, c);
    }
    close(fd[c][0]);
    while (wait(NULL) >= 0) {}
    return 0;
}
