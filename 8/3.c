#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    int r_time, w_time, wr_time;
    sscanf(argv[1], "%d", &r_time);
    sscanf(argv[2], "%d", &w_time);
    sscanf(argv[3], "%d", &wr_time);
    char c_t, c_v, prev_c = 0;
    int addr, size, value, prev_addr = 0, prev_size = 0;
    long long sum = 0;
    while (scanf("%c%c %x %d %d\n", &c_t, &c_v, &addr, &size, &value) == 5) {
        if (c_t == 'R') {
            if (prev_c == 'W' && prev_addr == addr && prev_size == size) {
                sum += wr_time - w_time;
            } else {
                sum += r_time;
            }
        } else {
            sum += w_time;
        }
        prev_c = c_t;
        prev_size = size;
        prev_addr = addr;
    }
    printf("%lld\n", sum);
    return 0;
}
