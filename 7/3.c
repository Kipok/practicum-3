#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>

typedef struct RandomOperations
{
    int (*next)(struct RandomOperations *);
} RandomOperations;

int next(RandomOperations *s)
{
    return rand();
}

RandomOperations *
random_create(const char *param)
{
    int seed;
    sscanf(param, "%d", &seed);
    srand(seed);
    RandomOperations *r = calloc(1, sizeof(RandomOperations));
    r->next = next;
    return r;
}
