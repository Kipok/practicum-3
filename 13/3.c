#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>

typedef struct memory_block
{
    long long sum;
} memory_block;

int
main(int argc, char *argv[])
{
    int key = ftok(argv[0], 42);
    int semid = semget(key, 1, 0600 | IPC_CREAT);
    struct sembuf cmd[2] = {{0, 1, 0}, {0, -1, 0}};
    semop(semid, cmd, 1);
    int shmid = shmget(key, sizeof(memory_block), 0600 | IPC_CREAT);
    memory_block *mb = (memory_block *) shmat(shmid, 0, 0); 
    mb->sum = 0;
    for (int i = 1; i < argc; i++) {
        if (!fork()) {
            FILE *fr = fopen(argv[i], "r");
            long long num = 0, sum = 0;
            while (fscanf(fr, "%lld", &num) == 1) {
                sum += num;
            }
            semop(semid, &cmd[1], 1);
            mb->sum += sum;
            semop(semid, cmd, 1);
            fclose(fr);
            return 0;
        }
    }
    while (wait(NULL) > 0) {}
    printf("%lld", mb->sum);
    semctl(semid, 1, IPC_RMID);
    shmctl(shmid, IPC_RMID, 0);
    return 0;
}
