#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h> 
#include <time.h>

int
main(int argc, char *argv[])
{
    int cnt, a1, d;
    sscanf(argv[1], "%d", &cnt);
    sscanf(argv[2], "%d", &a1);
    sscanf(argv[3], "%d", &d);
    for (int i = 0; i < cnt; i++) {
        printf("%d\n", a1 + d * i);
    }
    return 0;
}
