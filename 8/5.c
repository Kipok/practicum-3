#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

#define I_BYTE(n, i) (((n) & (0xFF << ((i) * 8))) >> ((i) * 8))


int
main(int argc, char *argv[])
{
    int memsize;
    sscanf(argv[1], "%d", &memsize);
    unsigned char *memory = calloc(memsize, sizeof(unsigned char));
    char *used = calloc(memsize, sizeof(char));
    char c_t, c_v;
    int addr, size, value; 
    while (scanf("%c%c %x %d %d\n", &c_t, &c_v, &addr, &size, &value) == 5) {
        for (int i = 0; i < size; i++) {
            memory[addr + i] = I_BYTE(value, size - 1 - i);
            used[addr + i] = 1;
        }
    }
    for (int i = 0; i < memsize; i++) {
        if (i % 16 == 0) {
            printf("%08X ", i);
        }
        if (!used[i]) {
            printf("??");
        } else {
            printf("%02X", memory[i]);
        }
        if (i % 16 == 15) {
            printf("\n");
        } else {
            printf(" ");
        }
    }
    free(memory);
    free(used);
    return 0;
}
