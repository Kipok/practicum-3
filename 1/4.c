#include <stdio.h>
#include <stdlib.h>

int 
main(void)
{
    freopen("input.txt", "rt", stdin);
    freopen("output.txt", "wt", stdout);
    int ca = 0, cb = 0, ra = 0, rb = 0, num = 0, rnum = 0, cr, pr, r = 0;
    if ((r = scanf("%d %d", &pr, &cr)) < 2) {
        if (r == 1)
            printf("0 1");
        else
            printf("0 0");
        return 0;
    }
    do {
        if (pr <= cr) {
            ++cb;
            ++num;
        } else {
            if (rnum < num) {
                ra = ca;
                rb = cb;
                rnum = num;
            }
            ca = cb = cb + 1;
            num = 0;
        }
        pr = cr;
    } while (scanf("%d", &cr) == 1);
    if (num > rnum) {
        ra = ca;
        rb = cb;
    }
    printf("%d %d", ra, rb + 1);
    return 0;
}