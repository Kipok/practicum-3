#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

char *
getlinell(int fd)
{
    char *s = (char *)malloc(sizeof(char));
    char c;
    int cnt = 0, sign = 0, sz = 1, i = 0;
    while ((cnt = read(fd, &c, 1)) > 0) {
        sign = 1;
        if (i >= sz) {
            sz <<= 1;
            s = (char *)realloc(s, sz * sizeof(char));
        }
        s[i++] = c;
        if (c == '\n')
            break;
    }
    if (!sign) {
        free(s);
        return NULL;
    }
    if (i >= sz) {
        sz <<= 1;
        s = (char *)realloc(s, sz * sizeof(char));
    }
    s[i++] = '\0';
    return s;
}

int 
main(void)
{
    char *s = getlinell(0);
    printf("%s", s);
    return 0;
}

