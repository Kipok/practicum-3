#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int 
get_rand(int low, int high)
{
    return (int)(floor(low + (rand() * 1.0) * (high - low) / ((long long)(RAND_MAX) + 1.0)));
}

int
main(void)
{
    int n, m, seed;
    int r[100];
    int p[100];
    scanf("%d", &n);
    for (int i = 0; i < n; i++) {
        scanf("%d%d", &r[i], &p[i]);
        p[i] += (i > 0) ? p[i - 1] : 0;
    }
    scanf("%d%d", &m, &seed);
    srand(seed);
    for (int i = 0; i < m; i++) {
        int rd = get_rand(0, 100);
        for (int j = 0; j < n; j++) {
            if (rd < p[j]) {
                printf("%d\n", r[j]);
                break;
            }
        }
    }
    return 0;
}
