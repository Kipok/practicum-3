/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 */


#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <limits.h>
#include <time.h>
#include <string.h>

enum
{
    MAGIC_1 = 'C',
    MAGIC_2 = 'M',
    MAGIC_3 = 'C',
    MAGIC_4 = '\x7f'
};

struct FHeader
{
    char    magic[4];
    int32_t partnum;
};

struct FSection
{
    int32_t offset;
    int32_t size;
};

//name num maxsize
int main(int argc, char **argv)
{
    if (argc < 4) {
        fprintf(stderr, "CMC binary file creator.\nUsage:\n\ncmcbincrt [file name] [section number] [maximum section size]\n\nCreates sections with random size from 1 to maximum section size filled with chars %s. Use \"xxd <created file>\" to view created binary file in hex.\n", "('0' + n % 10), where n - section number");
        return 1;
    }
    
    off_t *offsets;
    int32_t *sizes;
    char *section = malloc(1);
    int i, num, maxsize;
    int fd = open(argv[1], O_CREAT | O_TRUNC | O_WRONLY, 0777);
    if (fd < 0) return 2;
    
    num = atoi(argv[2]);
    maxsize = atoi(argv[3]);
    offsets = calloc(num, sizeof(off_t));
    sizes = calloc(num, sizeof(int32_t));
    
    struct FHeader header;
    struct FSection hsection;
    header.magic[0] = MAGIC_1;
    header.magic[1] = MAGIC_2;
    header.magic[2] = MAGIC_3;
    header.magic[3] = MAGIC_4;
    header.partnum = num;
    
    size_t wr = write(fd, &header, sizeof(header));
    if (wr < sizeof(header)) return 3;
    
    srand (time(NULL));
    
    for (i = 0; i < num; ++i) {
        offsets[i] = lseek(fd, 0, SEEK_CUR);
        if (offsets[i] < 0) return 4;
        
        sizes[i] = rand() % maxsize + 1;
        printf("section %d: %d bytes\n", i, sizes[i]);
        section = realloc(section, sizes[i]);
        memset(section, '0' + i % 10, sizes[i]);
        wr = write(fd, section, sizes[i]);
        if (wr < sizes[i]) return 5;
    }
    
    for (i = 0; i < num; ++i) {
        hsection.offset = offsets[i];
        hsection.size = sizes[i];
        if (write(fd, &hsection, sizeof(hsection)) < sizeof(hsection)) return 7;
    }
    
    free(offsets);
    free(sizes);
    close(fd);
	return 0;
}

