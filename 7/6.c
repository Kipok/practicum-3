#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

enum type {
    INIT,
    RD
};

enum mode {
    READ,
    WRITE
};

void
print_trace(int addr, int mode)
{
    if (mode == READ) {
        printf("RD %08x\n", addr);
    }
    if (mode == WRITE) {
        printf("WD %08x\n", addr);
    }
}

int 
get_rand(int low, int high)
{
    return (int)(floor(low + (rand() * 1.0) * (high - low) / ((long long)(RAND_MAX) + 1.0)));
}

int
prob_rand(int *p, int n)
{
    int rd = get_rand(0, 100);
    for (int j = 0; j < n; j++) {
        if (rd < p[j]) {
            return j;
        }
    }
    return 0;
}

int
main(void)
{
    int low, high, p1, p2, p3, count, seed;
    scanf("%x%x%d%d%d%d%d", &low, &high, &p1, &p2, &p3, &count, &seed);
    srand(seed);
    int p_init[3], p_read[2];
    p_read[0] = p3, p_read[1] = 100;
    p_init[0] = p1, p_init[1] = p_init[0] + p2, p_init[2] = 100;
    int state = INIT, addr = -1, cur_addr = 0;
    for (int i = 0; i < count; i++) {
        int act = 0;
        if (state == INIT) {
            act = prob_rand(p_init, 3);
            switch (act) {
            case 0: // write rand
                print_trace(get_rand(low, high), WRITE);
                break;
            case 1: // read rand
                print_trace(get_rand(low, high), READ);
                break;
            case 2: // read rand, goto read
                print_trace(cur_addr = get_rand(low, high), READ);
                state = RD;
                break;
            default:
                break;
            }
            continue;
        }
        if (state == RD) {
            if (addr == -1) {
                addr = cur_addr;
            }
            act = prob_rand(p_read, 2);
            switch(act) {
            case 0: // read 1 more
                print_trace(++addr, READ);
                break;
            case 1: // write cur_addr, goto init
                print_trace(cur_addr, WRITE);
                addr = -1;
                state = INIT;
            default:
                break;
            }
        }
    }
    return 0;
}
