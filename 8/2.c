#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define I_BYTE(n, i) (((n) & (0xFF << ((i) * 8))) >> ((i) * 8))
//#define GEN_TEST

int
read_buf(int fd, void *buf, int size, int offset)
{
    lseek(fd, offset, SEEK_SET);
    int cr = 0, all = 0;
    while ((cr = read(fd, (char *)buf + all, size - all)) > 0) {
        all += cr;
    }
    return all;
}

int
write_buf(int fd, void *buf, int size, int offset)
{
    lseek(fd, offset, SEEK_SET);
    int cr = 0, all = 0;
    while ((cr = write(fd, (char *)buf + all, size - all)) > 0) {
        all += cr;
    }
    return all;
}

enum {
    BUF_SIZE = 8000
};

int
main(int argc, char *argv[])
{  
#ifndef GEN_TEST
    int fd = open(argv[1], O_RDWR);
    int w_offset = lseek(fd, 0, SEEK_END);
    int file_size = w_offset;
    int r_offset = lseek(fd, 0, SEEK_SET);
    char buf[BUF_SIZE];
    while (1) {
        char n; // read one number
        int b = read_buf(fd, &n, 1, r_offset++);
        if (!b) {
            break;
        }
        int k = 0;
        read_buf(fd, &k, n, r_offset);
        if ((unsigned int)I_BYTE(k, n - 1) >= 0x80) { // make it negative
            for (int i = 3; i >= n; i--) {
                k ^= 0xFF << (i * 8);
            }
        }
        r_offset += n; // write one number
        n = 4;
        write_buf(fd, &n, 1, w_offset++);
        write_buf(fd, &k, 4, w_offset);
        w_offset += n;
        if (r_offset >= file_size) {
            break;
        }
    }
    int read_bytes = 0;
    w_offset = 0;
    r_offset = file_size;
    while ((read_bytes = read_buf(fd, buf, BUF_SIZE, r_offset)) > 0) {
        r_offset += read_bytes;
        write_buf(fd, buf, read_bytes, w_offset);
        w_offset += read_bytes;
    }
    if (ftruncate(fd, w_offset)) {
        return 1;
    }
    close(fd);
#else
    char c[19] = {1, 15, 1, 0, 1, -5, 1, 7};
    int fd = open("input1.txt", O_WRONLY | O_CREAT | O_TRUNC, 0777);
    write_buf(fd, c, 19, 0);
    close(fd);
#endif
    return 0;
}
