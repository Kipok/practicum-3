#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h> 

struct MyFile
{
    int fd;
    int size;
    char *buf;
    int num_read;
    int cr;
};

int
mygetc(struct MyFile *f)
{
    if (!f->num_read) {
        int cr = 0, all = 0;
        while ((cr = read(f->fd, f->buf + all, f->size - all)) > 0) {
            all += cr;
        }
        f->num_read = all;
    }
    if (!f->num_read) {
        return EOF;
    }
    unsigned char ans = f->buf[f->cr++];
    if (f->cr == f->num_read) {
        f->num_read = 0;
        f->cr = 0;
    }
    return ans;
}
