#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

enum {
    SIZE = 4096
};

int 
main(void)
{
    int cnt = 0, sign = 0;
    long long ans = 0, num = 0;
    char buff[SIZE];
    while ((cnt = read(0, buff, SIZE)) > 0) {
        for (int i = 0; i < cnt; i++) {
            if (buff[i] == '\t' || buff[i] == '\r' || buff[i] == '\n' || buff[i] == ' ') {
                if (!sign)
                    ans += num;
                else
                    ans -= num;
                num = sign = 0;
                continue;
            }
            if (buff[i] == '-') {
                sign = 1;
                continue;
            }
            if (buff[i] < '0' || buff[i] > '9') 
                continue;
            num *= 10ll;
            num += buff[i] - '0';
        }
    }
    ans += num;
    printf("%lld\n", ans);
    return 0;
}
