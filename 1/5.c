#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int 
main(int argc, char *argv[])
{
    FILE *in = fopen(argv[1], "rt");
    FILE *out = fopen(argv[2], "wt");
    double d = atof(argv[3]);
    double *arr = (double *)malloc(sizeof(double)), mean = 0;
    int sz = 1, cr = 0;
    while (fscanf(in, "%lf", &arr[cr++]) == 1)
    {
        if (cr == sz) {
            sz <<= 1;
            arr = (double *)realloc(arr, sz * sizeof(double));
        }
        mean += arr[cr - 1];
    }
    --cr;
    if (cr)
        mean /= cr;
    int mn = 0, mx = 0;
    if (cr == 0) {
        fprintf(out, "0\n0\n");
        return 0;
    }
    for (int i = 0; i < cr; i++) {
        if (fabs(arr[mx] - mean) < fabs(arr[i] - mean) + 1e-7) {
            if (fabs(fabs(arr[mx] - mean) - fabs(arr[i] - mean)) < 1e-7)
                mx = arr[mx] > arr[i] ? mx : i;
            else
                mx = i;
        }
        if (fabs(arr[i] - mean) < fabs(arr[mn] - mean) + 1e-7) {
            if (fabs(fabs(arr[mn] - mean) - fabs(arr[i] - mean)) < 1e-7)
                mn = arr[mn] > arr[i] ? mn : i;
            else
                mn = i;
        }
    }
    if (fabs(arr[mn] - mean) - d > 1e-7)
        fprintf(out, "0\n0\n");
    else
        fprintf(out, "%.10g\n%.10g\n", arr[mn], arr[mx]);
    fclose(in);
    fclose(out);
    return 0;
}