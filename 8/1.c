#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

#define I_BYTE(n, i) (((n) & (0xFF << ((i) * 8))) >> ((i) * 8))

int
read_buf(int fd, void *buf, int size, int offset)
{
    lseek(fd, offset, SEEK_SET);
    int cr = 0, all = 0;
    while ((cr = read(fd, (char *)buf + all, size - all)) > 0) {
        all += cr;
    }
    if (all != size)
        return 1;
    return 0;
}

int
write_buf(int fd, void *buf, int size, int offset)
{
    lseek(fd, offset, SEEK_SET);
    int cr = 0, all = 0;
    while ((cr = write(fd, (char *)buf + all, size - all)) > 0) {
        all += cr;
    }
    if (all != size)
        return 1;
    return 0;
}

int
main(int argc, char *argv[])
{  
    int fd = open(argv[1], O_RDWR);
    int w_offset = 0, r_offset = 0;
    const int low_bounds[3] = {-128, -32768, -2147483648};
    const int high_bounds[3] = {127, 32767, 2147483647};
    while (1) {
        char n; // read one number
        int b = read_buf(fd, &n, 1, r_offset++);
        if (b) {
            break;
        }
        int k = 0;
        read_buf(fd, &k, n, r_offset);
        r_offset += n; // write one number
        if ((unsigned int)I_BYTE(k, n - 1) >= 0x80) { // make it negative
            for (int i = 3; i >= n; i--) {
                k ^= 0xFF << (i * 8);
            }
        }
        for (int i = 0; i < 3; i++) {
            if (k >= low_bounds[i] && k <= high_bounds[i]) {
                n = i + 1;
                break;
            }
        }
        n += (n == 3);
        write_buf(fd, &n, 1, w_offset++);
        write_buf(fd, &k, n, w_offset);
        w_offset += n;
    }
    if (ftruncate(fd, w_offset))
        return 1;
    close(fd);
   /* 
    char c[23] = {4, -1, -1, -1, -1, 2, 78, 0, 4, 0, 0, 0, 0, 4, 1, 2, 3, 4, 4, 1, 1, 1, 0};
    int fd = open("input1.txt", O_WRONLY | O_CREAT);
    write_buf(fd, c, 23, 0);
    close(fd);*/
    return 0;
}
