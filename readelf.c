#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <elf.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>

enum {
    ERROR_OPENING_FILE,
    ERROR_READING_HEADER,
    ERROR_NOT_SUPPORTED_ELF,
    ERROR_NOT_AN_ELF
};

void 
print_error(int err)
{
    if (err == ERROR_OPENING_FILE) {
        fprintf(stderr, "Couldn't open input file\n");
    }
    if (err == ERROR_READING_HEADER) {
        fprintf(stderr, "Error while reading ELF Header\n");
    }
    if (err == ERROR_NOT_SUPPORTED_ELF) {
        fprintf(stderr, "Not supported ELF file\n");
    }
    if (err == ERROR_NOT_AN_ELF) {
        fprintf(stderr, "Not an ELF file\n");
    }
}

Elf32_Ehdr *
read_header(int fd)
{
    int cr = 0, all = 0;
    Elf32_Ehdr *hdr = (Elf32_Ehdr *)malloc(sizeof(Elf32_Ehdr));
    while ((cr = read(fd, (char *)hdr + all, sizeof(*hdr) - all)) > 0) {
        all += cr;
    }
    if (all != sizeof(*hdr)) {
        free(hdr);
        return NULL;
    }
    return hdr;
}

int 
check_header(Elf32_Ehdr *hdr)
{
    unsigned char *h = hdr->e_ident;
    if (h[0] != '\x7f' || h[1] != 'E' || h[2] != 'L' || h[3] != 'F') {
        print_error(ERROR_NOT_AN_ELF);
        return 1;
    }
    if (h[4] != 1 || h[5] != 1 || h[6] != 1 || h[7] != 0 || h[8] != 0 || h[9] != 0|| hdr->e_version != 1 || hdr->e_ehsize != 52 || (hdr->e_phentsize != 32 && hdr->e_phentsize != 0) || (hdr->e_shentsize != 40 && hdr->e_shentsize != 0)) {
        print_error(ERROR_NOT_SUPPORTED_ELF);
        return 1;
    }
    return 0;
}

char *
read_names(int fd, int offset, int size)
{
    lseek(fd, offset, SEEK_SET);
    char *names = (char *)malloc(size);
    int cr = 0, all = 0;
    while ((cr = read(fd, names + all, size - all)) > 0) {
        all += cr;
    }
    if (all != size) {
        free(names);
        return NULL;
    }
    return names;
}

Elf32_Shdr *
read_all_sections(int fd, int offset, int size)
{
    lseek(fd, offset, SEEK_SET);
    size *= sizeof(Elf32_Shdr);
    Elf32_Shdr *sections = (Elf32_Shdr *)malloc(size);
    int cr = 0, all = 0;
    while ((cr = read(fd, (char *)sections + all, size - all)) > 0) {
        all += cr;
    }
    if (all != size) {
        free(sections);
        return NULL;
    }
    return sections;
}

int
process_sections(int fd, Elf32_Ehdr *hdr)
{
    Elf32_Shdr *s = read_all_sections(fd, hdr->e_shoff, hdr->e_shnum);
    if (!s) {
        print_error(ERROR_NOT_AN_ELF);
        return 1;
    }
    char *names = read_names(fd, s[hdr->e_shstrndx].sh_offset, s[hdr->e_shstrndx].sh_size);
    if (!names) {
        print_error(ERROR_NOT_AN_ELF);
        free(s);
        return 1;
    }
    printf("                NAME       ADDR     OFFSET       SIZE   ALGN\n");
    for (int i = 1; i < hdr->e_shnum; i++) {
        if (s[i].sh_type == SHT_NULL) {
            continue;
        }
        printf("%20s 0x%08x %10d %10d 0x%04x\n", names + s[i].sh_name, s[i].sh_addr, s[i].sh_offset, s[i].sh_size, s[i].sh_addralign);
    }
    free(s);
    free(names);
    return 0;
}

int
print_type(int type)
{
    const char *types[5] = {"NONE", "REL", "EXEC", "DYN", "CORE"};
    if (type > 4) {
        return 1;
    }
    printf("TYPE: %s\n", types[type]);
    return 0;
}

int
main(int argc, char *argv[])
{
    if (argc < 2) {
        print_error(ERROR_OPENING_FILE);
        return 1;
    }
    int fd = open(argv[1], O_RDONLY); 
    if (fd < 0) {
        print_error(ERROR_OPENING_FILE);
        return 1;
    }
    Elf32_Ehdr *hdr = read_header(fd);
    if (!hdr) {
        print_error(ERROR_READING_HEADER);
        return 1;
    }
    if (check_header(hdr)) {
        free(hdr);
        return 1;
    }
    if (print_type(hdr->e_type)) {
        print_error(ERROR_NOT_SUPPORTED_ELF);
        free(hdr);
        return 1;
    }
    if (process_sections(fd, hdr)) {
        free(hdr);
        return 1;
    }
    free(hdr);
    close(fd);
    return 0;
}
