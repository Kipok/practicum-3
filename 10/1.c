#include <unistd.h>
#include <time.h>
#include <stdio.h>
#include <wait.h>

#define SUCCESS_EXIT(st) (WIFEXITED((st)) && !WEXITSTATUS((st)))

void
write_int(int fd, int *x)
{
    int cr = 0, all = 0;
    while ((cr = write(fd, (char *)x + all, sizeof(*x) - all)) > 0) {
        all += cr;
    }
}

void
read_int(int fd, int *x)
{
    int cr = 0, all = 0;
    while ((cr = read(fd, (char *)x + all, sizeof(*x) - all)) > 0) {
        all += cr;
    }
}

int
main(void)
{
    int fd[2];
    if (pipe(fd) < 0) {
        return 1;
    }
    time_t timer;
    time(&timer);
    struct tm *tmr = localtime(&timer);
    write_int(fd[1], &tmr->tm_sec);
    write_int(fd[1], &tmr->tm_min);
    write_int(fd[1], &tmr->tm_hour);
    close(fd[1]);
    if (!fork()) {
        if (!fork()) {
            if (!fork()) {
                int sec = 0;
                read_int(fd[0], &sec);
                printf("S:%02d\n", sec);
                close(fd[0]);
                return 0;
            }
            wait(NULL);
            int min = 0;
            read_int(fd[0], &min);
            printf("M:%02d\n", min);
            close(fd[0]);
            return 0;
        }
        wait(NULL);
        int hour = 0;
        read_int(fd[0], &hour);
        printf("H:%02d\n", hour);
        close(fd[0]);
        return 0;
    }
    wait(NULL);
    close(fd[0]);
    return 0;
}
