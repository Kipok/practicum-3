#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

char *
getlinell(int fd)
{
    char *s = (char *)malloc(sizeof(char));
    char c;
    int cnt = 0, sign = 0, sz = 1, i = 0;
    while ((cnt = read(fd, &c, 1)) > 0) {
        sign = 1;
        if (i >= sz) {
            sz <<= 1;
            s = (char *)realloc(s, sz * sizeof(char));
        }
        s[i++] = c;
        if (c == '\n')
            break;
    }
    if (!sign) {
        free(s);
        return NULL;
    }
    if (i >= sz) {
        sz <<= 1;
        s = (char *)realloc(s, sz * sizeof(char));
    }
    s[i++] = '\0';
    return s;
}

int 
isSpace(char c)
{
    return c == ' ' || c == '\t' || c == '\r' || c == '\n';
}

int 
main(void)
{
    char *s;
    double ans = 0, cur = 0;
    int i = 0, sign = 0;;
    while ((s = getlinell(0))) {
        sign = i = 0;
        ans = cur = 0;
        while (sscanf(s + i, "%lf", &cur) == 1) {
            sign = 1;
            ans += cur;
            while (isSpace(s[i++]));
            while (!isSpace(s[i++]));
        }
        if (!sign)
            printf("nan\n");
        else
            printf("%.10g\n", ans);
        free(s);
    }
    return 0;
}
