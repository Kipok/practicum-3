#include <stdio.h>
#include <unistd.h>
#include <wait.h>
#include <signal.h>

void
print_int(int fd, int *x)
{
    int all = 0, cr = 0;
    while ((cr = write(fd, (char *)x + all, sizeof(*x) - all)) > 0) {
        all += cr;
    }
}

int
read_int(int fd, int *x)
{
    int all = 0, cr = 0;
    while ((cr = read(fd, (char *)x + all, sizeof(*x) - all)) > 0) {
        all += cr;
    }
    if (all != sizeof(*x)) {
        return 1;
    }
    return 0;
}

int
main(int argc, char *argv[])
{
    int fd[2];
    int mod;
    long long max;
    sscanf(argv[2], "%d", &mod);
    sscanf(argv[3], "%lld", &max);
    if (pipe(fd) < 0) {
        return 1;
    }
    if (!fork()) { // uncle
        close(fd[1]);
        long long sum = 0;
        int cur = 0;
        int pid_nephew = 0;
        read_int(fd[0], &pid_nephew);
        while (!read_int(fd[0], &cur)) {
            sum += cur;
            if (sum > max) {
                printf("-1\n");
                kill(pid_nephew, SIGTERM);
                return 0;
            }
        }
        printf("%lld\n", sum);
        close(fd[0]);
        return 0;
    }
    close(fd[0]);
    if (!fork()) { // father
        if (!fork()) { // son
            int tmp = getpid();
            print_int(fd[1], &tmp);
            FILE *f = fopen(argv[1], "r");
            int sum;
            while (fscanf(f, "%d", &sum) == 1) {
                sum = (1ll * sum * sum) % mod;
                print_int(fd[1], &sum);
            }
            close(fd[1]);
            return 0;
        }
        close(fd[1]);
        wait(NULL);
        return 0;
    }
    close(fd[1]);
    while (wait(NULL) >= 0) {}
    printf("0\n");
    return 0;
}
