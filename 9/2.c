#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

#define SUCCESS_EXIT(st) (WIFEXITED((st)) && !WEXITSTATUS((st)))

int 
wait_and_exit(void)
{
    int st;
    wait(&st);
    if (SUCCESS_EXIT(st)) {
        exit(0);
    } else {
        exit(1);
    }
}

int
main(int argc, char *argv[])
{
    if (!fork()) {
        execlp(argv[1], argv[1], NULL);
    }
    int st = 0;
    wait(&st);
    if (SUCCESS_EXIT(st)) {
        if (!fork()) {
            execlp(argv[3], argv[3], NULL);
        }
        wait_and_exit();
    } else {
        if (!fork()) {
            execlp(argv[2], argv[2], NULL);
        }
        wait(&st);
        if (SUCCESS_EXIT(st)) {
            if (!fork()) {
                execlp(argv[3], argv[3], NULL);
            }
            wait_and_exit();
        } else {
            return 1;
        }
    }
    return 0;
}
