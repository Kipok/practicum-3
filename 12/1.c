#include <stdio.h>
#include <signal.h>
#include <wait.h>
#include <unistd.h>

volatile int want_bit = 0;
volatile int bit_is = 0;
volatile int bit_sent = 0;

void 
handler_father(int x)
{
    if (x == SIGUSR1) {
        want_bit = 1;
    }
}

void 
handler_son(int x)
{
    if (x == SIGUSR1) {
        bit_is = 0;
        bit_sent = 1;
    }
    if (x == SIGUSR2) {
        bit_is = 1;
        bit_sent = 1;
    }
}

int
main(void)
{
    signal(SIGUSR1, handler_father);
    int ppid = getpid(), spid = 0;
    if (!(spid = fork())) { // son
        signal(SIGUSR1, handler_son);
        signal(SIGUSR2, handler_son);
        while (1) {
            unsigned int i = 0;
            int n = 0;
            while (i < 32) {
                kill(ppid, SIGUSR1); // want bit
                while (!bit_sent) {}
                bit_sent = 0;
                n ^= (bit_is << i++);
            }
            printf("%d\n", n);
            fflush(stdout);
        }
        return 0;
    }
    int n;
    while (scanf("%d", &n) == 1) {
        unsigned int i = 0;
        while (i < 32) {
            if (want_bit) {
                want_bit = 0;
                kill(spid, (unsigned int)(n & (1 << i)) > 0 ? SIGUSR2 : SIGUSR1);
                ++i;
            }
        }
    }
    while (!want_bit) {}
    kill(spid, SIGINT);
    wait(NULL);
    return 0;
}
