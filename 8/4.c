#include <stdio.h>
#include <stdlib.h>

int
main(int argc, char *argv[])
{
    int block_size, memory_size, cache_size;
    sscanf(argv[1], "%d", &memory_size);
    sscanf(argv[2], "%d", &cache_size);
    sscanf(argv[3], "%d", &block_size);
    int block_count = cache_size / block_size, count = 0;
    int *cache = (int *)calloc(block_count, sizeof(int));
    char *used = (char *)calloc(block_count, sizeof(char));
    char c_t, c_v;
    int addr, size, value;
    while (scanf("%c%c %x %d %d\n", &c_t, &c_v, &addr, &size, &value) == 5) { 
        int mem_num = addr / block_size;
        int num = mem_num % block_count;
        if (used[num] && cache[num] != mem_num) {
            ++count;
        }
        cache[num] = mem_num;
        used[num] = 1;
    }
    printf("%d\n", count);
    free(cache);
    free(used);
    return 0;
}
