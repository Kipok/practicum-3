#include <unistd.h>
#include <stdio.h>
#include <wait.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/shm.h>

typedef struct memory_block 
{
    int num;
} memory_block;


int
main(int argc, char *argv[])
{
    int nproc, key, maxval;
    sscanf(argv[1], "%d", &nproc);
    sscanf(argv[2], "%d", &key);
    sscanf(argv[3], "%d", &maxval);

    int semid = semget(key, 1, 0600 | IPC_CREAT);
    struct sembuf cmd[2] = {{0, 1, 0}, {0, 0, 0}};
    semop(semid, cmd, 1);

    int shmid = shmget(key, sizeof(memory_block), 0600 | IPC_CREAT);
    memory_block *mb = shmat(shmid, 0, 0);
    mb->num = 0;
    for (int i = 1; i <= nproc; i++) {
        if (!fork()) {
            while (1) {
                cmd[0].sem_op = -i;
                if (semop(semid, cmd, 2) < 0) { // down
                    return 0;
                }
                if (mb->num >= maxval) {
                    semctl(semid, 1, IPC_RMID);
                    shmctl(shmid, IPC_RMID, 0);
                    return 0;
                }
                printf("%d: %d\n", i - 1, mb->num);
                fflush(stdout);
                ++mb->num;
                cmd[0].sem_op = (1ll * mb->num * mb->num) % nproc + 1;
                semop(semid, cmd, 1); // up
            }
        }
    }
    while (wait(NULL) > 0) {}    
    return 0;
}
