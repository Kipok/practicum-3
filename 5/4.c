#include <stdio.h>
#include <stdlib.h>

typedef struct SequenceOperations {
    int d;
    int a1;
    int cur;
    int (*next)(struct SequenceOperations *);
} SequenceOperations;

int next(SequenceOperations *s)
{
    return (s->cur++) * s->d + s->a1;
}

SequenceOperations *
sequence_create(const char *param_file)
{
    FILE *f = fopen(param_file, "rt");
    SequenceOperations *s = malloc(sizeof(SequenceOperations));
    fscanf(f, "%d%d", &s->a1, &s->d);
    s->cur = 0;
    s->next = next;
    fclose(f);
    return s;
}
