#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

int
main(int argc, char *argv[])
{
    int count, low, high, seed;
    sscanf(argv[1], "%d", &count);
    sscanf(argv[2], "%d", &low);
    sscanf(argv[3], "%d", &high);
    sscanf(argv[4], "%d", &seed);
    srand(seed);
    for (int i = 0; i < count; i++) {
        double rnd = floor(low + (rand() * 1.0) * (high - low) / ((long long)(RAND_MAX) + 1.0));
        printf("%d\n", (int)rnd);
    }
    return 0;
}
