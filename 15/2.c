#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


enum
{
    MAX_SIZE = 1000001
};

pthread_t threads[MAX_SIZE];
char str[MAX_SIZE];

void *
tokenize(void *ptr)
{
    char *s = (char *)ptr;
    char **save_ptr;
    char *cur, *min = strtok_r(s, ",", save_ptr);
    while (cur = strtok_r(NULL, ",", save_ptr)) {
        if (strcmp(cur, min) < 0) {
            min = cur;
        }
    }
    return (void *)min;
}

int
main(int argc, char *argv[])
{
    int i = 0;
    char *ptr = str;
    while (fgets(ptr, MAX_SIZE, stdin) != NULL) {
        if (ptr[strlen(ptr) - 1] == '\n') {
            ptr[strlen(ptr) - 1] = '\0';
        }
        pthread_create(threads + i, NULL, (void *)tokenize, (void*) ptr); 
        ptr += strlen(str) + 1;
        ++i;
    }
    char *s;
    for (int j = 0; j < i; j++) {
        pthread_join(threads[j], (void **)&s);
        printf("%s\n", s);
    }
    return 0;
}
