#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/types.h> 

unsigned long long
search_dir(char *name)
{
    DIR *d = opendir(name);
    if (!d) {
        return 0;
    }
    struct dirent *dir;
    struct stat info;
    int ct = 0, sz = 1;
    unsigned long long count = 0;
    char **names = (char **)malloc(sizeof(char *));
    while ((dir = readdir(d))) {
        if (!strcmp(dir->d_name, ".") || !strcmp(dir->d_name, "..")) {
            continue;
        }
        if (ct == sz) {
            sz <<= 1;
            names = (char **)realloc(names, sz * sizeof(char *));
        }
        names[ct++] = strdup(dir->d_name);
    }
    closedir(d);
    for (int i = 0; i < ct; i++) {
        int length = strlen(names[i]) + strlen(name) + 2;
        char *file_name = (char *)malloc(length);
        snprintf(file_name, length, "%s/%s", name, names[i]);
        int ret; 
        if ((ret = lstat(file_name, &info)) == 0 && S_ISREG(info.st_mode) && !S_ISLNK(info.st_mode)) {
            count += info.st_size;
        }
        if (S_ISDIR(info.st_mode)) {
            count += search_dir(file_name);
        } 
        free(file_name);
        free(names[i]);
    }
    free(names);
    return count;
}

int
main(int argc, char *argv[])
{
    unsigned long long cnt = search_dir(argv[1]);
    printf("%llu\n", cnt);
    return 0;
}
