#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h> 
#include <time.h>

struct tm
get_time(FILE *f)
{
    struct tm t;
    if (fscanf(f, "%d/%d/%d %d:%d:%d", &t.tm_year, &t.tm_mon, &t.tm_mday, &t.tm_hour, &t.tm_min, &t.tm_sec) == 6) {
        t.tm_year -= 1900;
        t.tm_mon -= 1;
        t.tm_isdst = -1;
        t.tm_wday = t.tm_yday = 0;
        return t;
    }
    t.tm_year = -1;
    return t;
}

int
main(int argc, char *argv[])
{
    FILE *f = fopen(argv[1], "rt");
    struct tm t_pr = get_time(f);
    do {
        struct tm t_cr = get_time(f);
        if (t_cr.tm_year != -1)
            printf("%lld\n", (long long)difftime(mktime(&t_cr), mktime(&t_pr)));
        t_pr = t_cr;
    } while(t_pr.tm_year != -1);
    return 0;
}
