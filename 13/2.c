#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>

typedef struct sembuf sembuf;

int
main(int argc, char *argv[])
{
    int fd[2];
    if (pipe(fd) < 0) {
        return -1;
    }
    int max_val;
    sscanf(argv[1], "%d", &max_val);

    if (max_val <= 1) {
        printf("Done\n");
        return 0;
    }

    sembuf sup1[1] = {{0, 2, 0}};
    sembuf sdown1[1] = {{0, 0, 0}};
    sembuf sup2[1] = {{0, -1, 0}};
    sembuf sdown2[1] = {{0, -1, 0}};
    key_t semkey;
    int semid, value = 0;
    
    FILE *fr = fdopen(fd[0], "r");
    FILE *fw = fdopen(fd[1], "w");
    fprintf(fw, "1\n");
    fflush(fw);

    if (!fork()) {
        semkey = ftok(argv[0], 42);
        semid = semget(semkey, 1, IPC_CREAT | 0666);
        
        while (fscanf(fr, "%d", &value) == 1 && value < max_val) {
            printf("1 %d\n", value);
            fflush(stdout);
            fprintf(fw, "%d\n", value + 1);
            fflush(fw);
            semop(semid, sup1, 1);
            if (semop(semid, sdown1, 1) < 0) {
                break;
            }
        }
        semctl(semid, 0, IPC_RMID);
        fclose(fr);
        fclose(fw);
        return 0;
    }
    if (!fork()) {
        semkey = ftok(argv[0], 42);
        semid = semget(semkey, 1, IPC_CREAT | 0666);

        if (semop(semid, sdown2, 1) >= 0) { 
            while (fscanf(fr, "%d", &value) == 1 && value < max_val) {
                printf("2 %d\n", value);
                fflush(stdout);
                fprintf(fw, "%d\n", value + 1);
                fflush(fw);
                semop(semid, sup2, 1);
                if (semop(semid, sdown2, 1) < 0) {
                    break;
                }
            }
        }
        semctl(semid, 0, IPC_RMID);
        fclose(fr);
        fclose(fw);
        return 0;
    }
    fclose(fw);
    fclose(fr);
    while (wait(NULL) > 0) {}
    printf("Done\n");
    return 0;
}
