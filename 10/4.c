#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <wait.h>

#define SUCCESS_EXIT(st) (WIFEXITED((st)) && !WEXITSTATUS((st)))

int
main(int argc, char *argv[])
{
    int fd[2];
    if (pipe(fd) < 0) {
        return 1;
    }
    if (!fork()) { // (p1 a1 2>>a2 || p2 < a2)
        dup2(fd[1], 1);
        close(fd[0]);
        close(fd[1]);
        if (!fork()) { // p1 a1 2>>a2
            if (!freopen(argv[7], "a", stderr)) { // 2 >> a2
                return 1;
            }
            execlp(argv[1], argv[1], argv[6], NULL);
        }
        int st;
        wait(&st);
        if (!SUCCESS_EXIT(st) && !fork()) { // p2 < a2
            if (!freopen(argv[7], "r", stdin)) {
                return 1;
            }
            execlp(argv[2], argv[2], NULL);
        }
		if (!SUCCESS_EXIT(st)) {
		    wait(NULL);
		}
        return 0;
    }
    if (!fork()) { // (p3; p4; p5)
        dup2(fd[0], 0);
        close(fd[0]);
        close(fd[1]);
        for (int i = 3; i <= 5; i++) {
            if (!fork()) {
                execlp(argv[i], argv[i], NULL);
            }
            wait(NULL);
        }
		return 0;
    }
    close(fd[0]);
    close(fd[1]);
    while (wait(NULL) >= 0) {}
    return 0;
}
