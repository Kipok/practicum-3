#include <unistd.h>
#include <stdio.h>
#include <fcntl.h>
#include <sys/wait.h>

int
main(int argc, char *argv[])
{
    FILE *f = fopen("tmp", "w+");
    for (int i = 1; i < argc; i++) {
        if (!fork()) {
            FILE *c_f = fopen(argv[i], "r");
            int cur = 0, sum = 0;
            while (fscanf(c_f, "%d", &cur) == 1) {
                sum += cur;
            }
            fseek(c_f, (i - 1) * (sizeof(sum) + 1), SEEK_SET);
            fprintf(f, "%d ", sum);
            fclose(c_f);
            return 0;
        }
    }
    while (wait(NULL) >= 0) {}
    int ans = 0, cur = 0;
    fseek(f, 0, SEEK_SET);
    while (fscanf(f, "%d", &cur) == 1) {
        ans += cur;
    }
    printf("%d\n", ans);
    fclose(f);
    unlink("tmp");
    return 0;
}
