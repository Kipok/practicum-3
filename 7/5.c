#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

enum {
    READ,
    WRITE
};

void
print_trace(int addr, int mode)
{
    if (mode == READ) {
        printf("RD %08x\n", addr);
    }
    if (mode == WRITE) {
        printf("WD %08x\n", addr);
    }
}

void 
dumb(int low, int high, int *arr, int addr)
{
    int i = low, j = high, pivot, t;
    if (low + 1 >= high) {
        return;
    }
    t = (low + high) / 2;
    pivot = arr[t];
    print_trace(addr + t * 4, READ);
    while (i < j) {
        print_trace(addr + i * 4, READ);
        if (arr[i] >= pivot && arr[j - 1] <= pivot) {
            print_trace(addr + (j - 1) * 4, READ);
            print_trace(addr + i * 4, READ);
            t = arr[i];
            print_trace(addr + (j - 1) * 4, READ);
            print_trace(addr + i * 4, WRITE);
            arr[i] = arr[j - 1];
            print_trace(addr + (j - 1) * 4, WRITE);
            arr[j - 1] = t;
            ++i;
            --j;
        } else {
            if (arr[i] >= pivot) { // my check
                print_trace(addr + (j - 1) * 4, READ);
            }
            print_trace(addr + i * 4, READ);
            if (arr[i] <= pivot) {
                ++i;
            }
            print_trace(addr + (j - 1) * 4, READ);
            if (arr[j - 1] >= pivot) {
                --j;
            }
        }
    }
    dumb(low, j, arr, addr);
    dumb(j, high, arr, addr);
}

int
main(int argc, char *argv[])
{
    int addr, n;
    sscanf(argv[1], "%x", &addr);
    sscanf(argv[2], "%d", &n);
    int *arr = calloc(n, sizeof(int));
    for (int i = 0; i < n; i++) {
        scanf("%d", &arr[i]);
    }
    dumb(0, n, arr, addr);
    free(arr);
    return 0;
}
