#include <unistd.h>
#include <wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <sys/msg.h>

struct msg
{
    long mtype;
    int value;
};

int
main(int argc, char *argv[])
{
    int nproc, key, maxval;
    sscanf(argv[1], "%d", &nproc);
    sscanf(argv[2], "%d", &key);
    sscanf(argv[3], "%d", &maxval);
    int msgid = msgget(key, 0666 | IPC_CREAT);
    struct msg message = {nproc, 0};
    msgsnd(msgid, &message, sizeof(message.value), 0);
    for (int i = 1; i <= nproc; i++) {
        if (!fork()) {
            while (1) {
                if (msgrcv(msgid, &message, sizeof(message.value), i == 1 ? nproc : i - 1, 0) < 0) {
                    return 0;
                }
                if (message.value >= maxval) {
                    message.mtype = nproc + 1;
                    msgsnd(msgid, &message, sizeof(message.value), 0);
                    return 0;
                }
                printf("%d %d\n", i, message.value);
                fflush(stdout);
                message.mtype = i;
                ++message.value;
                msgsnd(msgid, &message, sizeof(message.value), 0);
            }
            return 0;
        }
    }
    msgrcv(msgid, &message, sizeof(message.value), nproc + 1, 0);
    msgctl(msgid, IPC_RMID, 0);
    while (wait(NULL) > 0) {}
    return 0;
}
