#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>
#include <stdlib.h>

#define SUCCESS_EXIT(st) (WIFEXITED((st)) && !WEXITSTATUS((st)))

int 
wait_and_exit(void)
{
    int st;
    wait(&st);
    if (SUCCESS_EXIT(st)) {
        exit(0);
    } else {
        exit(1);
    }
}

extern char **environ;

int
main(int argc, char *argv[])
{
    int st = 0;
    if (!fork()) { // execute this process in order not to change environment
        if (!fork()) {
            execlp(argv[1], argv[1], NULL);
        }
        wait(&st);
        if (!SUCCESS_EXIT(st)) {
            if (!fork()) {
                execlp(argv[2], argv[2], NULL);
            }
        } else {
            return 0;
        }
        wait_and_exit();
    }
    wait(&st);
    if (SUCCESS_EXIT(st)) {
        if (!fork()) {
            execlp(argv[3], argv[3], NULL);
        }
        wait_and_exit();
    } else {
        return 1;
    }
    return 0;
}
