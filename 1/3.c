#include <stdio.h>
#include <stdlib.h>

struct node
{
    struct node *left, *right;
    int key;
    char data;
};

void f(int x)
{
    x = x;
}

void processTree(struct node *pnode, void *func(int x))
{
    struct node **stack = (struct node **)malloc(sizeof(struct node *));
    int sz = 1, cr = -1;
    while (pnode || cr >= 0) {
        if (cr >= 0) {
            pnode = stack[cr--];
            func(pnode->key);
            if (pnode->right) 
                pnode = pnode->right;
            else 
                pnode = NULL;
        }
        while (pnode) {
            if (++cr >= sz) {
                sz <<= 1;
                stack = (struct node **)realloc(stack, sz * sizeof(struct node *));
            }
            stack[cr] = pnode;
            pnode = pnode->left;
        }
    }
}


int 
main(void)
{
    freopen("output.txt", "wt", stdout);
	node *t = (node *)malloc(sizeof(node *));
    node *c = t;
    t->key = 1;
    t->left = (node *)malloc(sizeof(node *));
    t->left->key = 2;
    t->right = (node *)malloc(sizeof(node *));
    t->right->key = 3;
    t->right->left = t->right->right = 0;
    t = t->left;
    t->left = (node *)malloc(sizeof(node *));
    t->left->key = 4;
    t->right = (node *)malloc(sizeof(node *));
    t->right->key = 5;
    t->left->left = t->left->right = 0;
    t = t->right;
    t->left = (node *)malloc(sizeof(node *));
    t->left->key = 6;
    t->right = (node *)malloc(sizeof(node *));
    t->right->key = 7;
    t->left->left = t->left->right = 0;
    t->right->left = t->right->right = 0;
    processTree(c);
    return 0;
}