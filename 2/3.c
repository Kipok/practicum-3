#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int
read_int(int fd, int *x)
{
    int cr = 0, c_al = 0;
    while ((cr = read(fd, (char *)(x) + cr, sizeof(*x) - cr)) > 0) {
        c_al += cr;
    }
    if (c_al != sizeof(*x))
        return -1;
    return 0;
}

int
write_int(int fd, int x)
{
    int cr = 0, c_al = 0;
    while ((cr = write(fd, (char *)(&x) + cr, sizeof(x) - cr)) > 0) {
        c_al += cr;
    }
    if (c_al != sizeof(x))
        return -1;
    return 0;
}

int
my_dump_tree(const struct node *root, int fd, int pos)
{
    if (!root) {
        return 0;
    }
    lseek(fd, 0, SEEK_END);
    if (write_int(fd, root->key))
        return -1;
    if (root->left) {
        if (write_int(fd, pos + 3 * sizeof(int)))
            return -1;
    } else {
        if (write_int(fd, 0))
            return -1;
    }
    if (write_int(fd, 0))
        return -1; 
    if (root->left) {
        if (my_dump_tree(root->left, fd, pos + 3 * sizeof(int)))
            return -1;
    }
    int offset = lseek(fd, 0, SEEK_END);
    lseek(fd, pos + 2 * sizeof(int), SEEK_SET);
    if (root->right) {
        if (write_int(fd, offset))
            return -1;
    }
    if (root->right) {
        if (my_dump_tree(root->right, fd, offset))
            return -1;
    }
    return 0;
}

int 
dump_tree(const struct node *root, int fd)
{
    if (my_dump_tree(root, fd, 0))
        return -1;
    return 0;
}

struct node *
my_load_tree(int fd, int offset)
{
    lseek(fd, offset, SEEK_SET);
    struct node *root = (struct node *)malloc(sizeof(struct node));
    root->left = root->right = NULL;
    if (read_int(fd, &root->key)) {
        free(root);
        return NULL;
    }
    int l_o = 0, r_o = 0;
    if (read_int(fd, &l_o)) {
        free(root);
        return NULL;
    }
    if (read_int(fd, &r_o)) {
        free(root);
        return NULL;
    }
    if (l_o != 0) {
        if (!(root->left = my_load_tree(fd, l_o))) {
            free(root);
            return NULL;
        }
    }
    if (r_o != 0) {
        if (!(root->right = my_load_tree(fd, r_o))) {
            free(root);
            return NULL;
        }
    }
    return root;
}

struct node * 
load_tree(int fd)
{
    struct node *tree;
    if ((tree = my_load_tree(fd, 0)))
        return tree;
    return NULL;
}
