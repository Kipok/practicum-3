#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char buff[100];

int 
main(int argc, char *argv[])
{
    FILE *src = fopen(argv[1], "rt");
    FILE *dst = fopen(argv[2], "wt");
    while (fgets(buff, 99, src) != NULL)
    {
        int d = strlen(buff);
        if (buff[d - 1] == '\n') {
            buff[d - 1] = '\0';
            fprintf(dst, "%s#\n", buff);
        } else {
            fprintf(dst, "%s", buff);
        }
    }
   // fputc('#', dst);
    fclose(src);
    fclose(dst);
    return 0;
}