#include <unistd.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>

void
piping(int num, int *fd_12, int *fd_21, int *fd_p, int max)
{
    if (num == 1) {
        dup2(fd_12[1], 1);
        dup2(fd_21[0], 0);
    } else {
        dup2(fd_12[0], 0);
        dup2(fd_21[1], 1);
    }
    FILE *fp = fdopen(fd_p[1], "w");
    close(fd_p[0]);
    close(fd_12[0]);
    close(fd_21[1]);
    close(fd_12[1]);
    close(fd_21[0]);
    if (max <= 1) {
        fclose(fp);
        close(fd_p[1]);
        return;
    }
    if (num == 2) {
        printf("1\n");
        fflush(stdout);
    }
    int n;
    while (scanf("%d", &n) == 1 && n < max) {
        printf("%d\n", n + 1);
        fflush(stdout);
        fprintf(fp, "%d %d\n", num, n);
        fflush(fp);
    }
    fclose(fp);
    close(fd_p[1]);
}

int
main(int argc, char *argv[])
{
    int fd_12[2], fd_21[2], fd_p[2];
    if (pipe(fd_12) < 0 || pipe(fd_21) < 0 || pipe(fd_p) < 0) {
        return 1;
    }
    int max;
    sscanf(argv[1], "%d", &max);
    for (int i = 1; i <= 2; i++) {
        if (!fork()) {
            piping(i, fd_12, fd_21, fd_p, max);
            return 0;
        }
    }
    dup2(fd_p[0], 0);
    close(fd_p[0]);
    close(fd_p[1]);
    close(fd_12[0]);
    close(fd_12[1]);
    close(fd_21[0]);
    close(fd_21[1]);
    int num, val;
    while (scanf("%d %d", &num, &val) == 2) {
        printf("%d %d\n", num, val);
    }
    while (wait(NULL) >= 0) {}
    printf("Done\n");
    return 0;
}
