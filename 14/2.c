#include <unistd.h>
#include <wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <signal.h>

#define SIZE(msg) (sizeof(msg) - sizeof(msg.mtype))
#define LEFT(x) ((x) == 1 ? nproc : (x) - 1)
#define RIGHT(x) ((x) == nproc ? 1 : (x) + 1)

int 
get_rand(int low, int high)
{
    return (int)((low + (rand() * 1.0) * (high - low) / ((long long)(RAND_MAX) + 1.0)));
}

volatile int f;

void
term_handler(int x)
{
    if (x == SIGINT) {
        f = 1;
    }
}

enum {
    SEC_CONV = 100000
};

struct msg
{
    long mtype;
    int pid;
    char state;
};

void 
handle_message(int msgid, struct msg message, char *states, int nproc)
{
    states[message.pid] = message.state;
    printf("%s\n", states + 1);
    for (int i = 1; i <= nproc; i++) {
        if (states[i] == 'W' && states[LEFT(i)] != 'E' && states[RIGHT(i)] != 'E') {
            message.mtype = i;
            msgsnd(msgid, &message, SIZE(message), 0);
            states[i] = 'E';
            printf("%s\n", states + 1);
        }
    }
}

void 
do_this_funny_work(int pid, int nproc, int msgid)
{
    struct msg message;
    while (1) {
        int n_doing = get_rand(1, 10) * SEC_CONV;
        usleep(n_doing);                                // doing nothing

        message.mtype = nproc + 1;
        message.pid = pid;
        message.state = 'W';
        msgsnd(msgid, &message, SIZE(message), 0);

        msgrcv(msgid, &message, SIZE(message), pid, 0); // wait for forks

        int eat = get_rand(1, 10) * SEC_CONV;
        usleep(eat);                                    // eat time

        message.mtype = nproc + 1;
        message.pid = pid;
        message.state = 'I';
        msgsnd(msgid, &message, SIZE(message), 0);
    }
}

int
main(int argc, char *argv[])
{
    int key, nproc, seed;
    sscanf(argv[1], "%d", &key);
    sscanf(argv[2], "%d", &nproc);
    sscanf(argv[3], "%d", &seed);
    srand(seed);
    char *states = calloc(nproc + 2, sizeof(char));
    states[nproc + 1] = '\0';
    for (int i = 1; i <= nproc; i++) {
        states[i] = 'I';
    }
    int msgid = msgget(key, 0600 | IPC_CREAT);
    struct msg message;

    signal(SIGINT, term_handler);
    int *pids = calloc(nproc + 1, sizeof(int));
    for (int i = 1; i <= nproc; i++) {
        if (!(pids[i] = fork())) {
            do_this_funny_work(i, nproc, msgid);
            return 0;
        }
    }
    while (!f) {
        msgrcv(msgid, &message, SIZE(message), 0, 0);
        handle_message(msgid, message, states, nproc);
    }
    for (int i = 1; i <= nproc; i++) {
        kill(pids[i], SIGTERM);
    }
    while (wait(NULL) > 0) {}
    free(pids);
    free(states);
    msgctl(msgid, IPC_RMID, 0);
    return 0;
}
