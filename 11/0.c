#include <unistd.h>
#include <wait.h>
#include <signal.h>
#include <stdio.h>

volatile int tp = 0, tw = 0, tk = 0;

void 
onPrint(int x)
{
    if (x == SIGUSR1) {
        if (!tp) {
            tp = 1; // print value
        } else {
            if (!tw) {
                tw = 1; // wake second son or kill all
            } else {
                tk = 1;
            }
        }
    }
}

int
main(void) // 3
{
    int son = 0, grandf = getpid();
    signal(SIGUSR1, onPrint);
    if (!(son = fork())) { // 2
        int son1, son2;
        if (!(son2 = fork())) { // 4
            while (1) {
                if (tp) {
                    printf("4");
					fflush(stdout);
                    kill(grandf, SIGUSR1);
                    break;
                }
            }
            while (1) {}
        }
        if (!(son1 = fork())) { // 1
            printf("1");
            fflush(stdout);
            kill(getppid(), SIGUSR1);
            while (1) {}
        }
        while (1) {
            if (tp) {
                printf("2");
                fflush(stdout);
                kill(getppid(), SIGUSR1);
                break;
            }
        }
        while (1) {
            if (tw) {
                kill(son2, SIGUSR1);
                break;
            }
        }
        while (1) {
            if (tk) {
                kill(son1, SIGINT);
                kill(son2, SIGINT);
                while (wait(NULL) >= 0) {}
                return 0;
            }
        }
    }
    while (1) {
        if (tp) {
            printf("3");
            fflush(stdout);
            kill(son, SIGUSR1);
            break;
        }
    }
    while (1) {
        if (tw) {
            kill(son, SIGUSR1); // kill all
            wait(NULL);
            return 0;    
        }
    }
    return 0;
}
