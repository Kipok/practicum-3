#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

void *
sum_current(void *ptr)
{
    char *path = (char *)ptr;
    FILE *f = fopen(path, "r");
    int sum = 0, cur = 0;
    while (fscanf(f, "%d", &cur) == 1) {
        sum += cur;
    }
    fclose(f);
    return (void *)sum;
}

int
main(int argc, char *argv[])
{
    pthread_t *threads = (pthread_t *)calloc(argc, sizeof(pthread_t));
    for (int i = 1; i < argc; i++) {
       pthread_create(threads + i - 1, NULL, (void *)sum_current, (void*) argv[i]);
    }
    int sum = 0, cur = 0;
    for (int i = 0; i < argc - 1; i++) {
        pthread_join(threads[i], (void **)&cur);
        sum += cur;
    }
    printf("%d\n", sum);
    free(threads);
    return 0;
}
