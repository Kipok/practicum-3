#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

char buff[1000001];
unsigned int b[30];

int 
main(void)
{
	fgets(buff, 1000001, stdin);
    if (buff[strlen(buff) - 1] == '\n')
        buff[strlen(buff) - 1] = '\0';
    FILE *src = fopen(buff, "rt");
	char c;
	while ((c = getc(src)) != EOF)
	{
		c = tolower(c);
        if (c >= 'a' && c <= 'z')
            ++b[c - 'a'];
	}
    for (int i = 0; i < 26; i++)
    {
        if (b[i] > 0)
            printf("%c %u\n", (char)('a' + i), b[i]);
    }
    fclose(src);
    return 0;
}