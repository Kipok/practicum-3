#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h> 

struct FHeader
{
    char magic[4];
    int32_t partnum;
};

struct FSection
{
    int32_t offset;
    int32_t size;
};

enum {
    BUF_SIZE = 1000
};

int 
min (int a, int b)
{
    return a < b ? a : b;
}

void 
print_buf (void *buf, int size, int fd)
{
    int cr = 0, all = 0;
    while ((cr = write(fd, (char *)buf + all, size - all)) > 0) {
        all += cr;
    }
}

void 
read_buf(void * buf, int size, int fd)
{
    int cr = 0, all = 0;
    while ((cr = read(fd, (char *)buf + all, size - all)) > 0) {
        all += cr;
    }
}

void
print_part(struct FSection prt, int fd_r, int fd_w)
{
    lseek(fd_r, prt.offset, SEEK_SET);
    int cr_r = 0;
    char *buf = (char *)malloc(BUF_SIZE);
    while ((cr_r = read(fd_r, buf, min(prt.size, BUF_SIZE))) > 0) {
        print_buf(buf, cr_r, fd_w);
        prt.size -= cr_r;
    }
    free(buf);
}

void
print_file(int st, int num, int fd_r, int fd_w)
{
    struct FHeader hd;
    hd.magic[0] = 'C', hd.magic[1] = 'M', hd.magic[2] = 'C', hd.magic[3] = '\x7f';
    hd.partnum = num;
    print_buf(&hd, sizeof(struct FHeader), fd_w);
    struct FSection *sect = malloc(sizeof(struct FSection));
    int cr = 0, sz = 1;
    for (int i = 0; i < num; i++) {
        lseek(fd_r, - sizeof(struct FSection) * (st - i), SEEK_END);
        if (cr == sz) {
            sz <<= 1;
            sect = realloc(sect, sizeof(struct FSection) * sz);
        }
        read_buf(sect + cr, sizeof(struct FSection), fd_r);
        print_part(sect[cr], fd_r, fd_w);
        sect[cr].offset = lseek(fd_w, 0, SEEK_END) - sect[cr].size;
        ++cr;
    }
    print_buf(sect, sizeof(struct FSection) * cr, fd_w);
    free(sect);
}

int
main (int argc, char *argv[])
{
    int fd_r = open(argv[1], O_RDONLY);
    int num;
    sscanf(argv[2], "%d", &num);
    struct FHeader hd;
    read_buf(&hd, sizeof(struct FHeader), fd_r);
    if (hd.partnum < num) {
        num = hd.partnum;
        if (num == 0) {
            close(fd_r);
            return 0;
        }
    }
    int mod = hd.partnum % num, num_sct = hd.partnum / num, prd = 0;
    for (int i = 1; i <= num; i++) {
        int num_prt = num_sct + ((mod--) > 0);
        int length = strlen(argv[1]) + 20;
        char *file_name = malloc(length);
        snprintf(file_name, length, "%s-%d", argv[1], i);
        int fd_w = open(file_name, O_WRONLY | O_TRUNC | O_CREAT, 0777);
        print_file(hd.partnum - prd, num_prt, fd_r, fd_w);
        prd += num_prt;
        close(fd_w);
        free(file_name);
    }
    close(fd_r);
    return 0;
}
