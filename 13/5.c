#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <wait.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <sys/types.h>

int 
get_rand(int low, int high)
{
    return (int)((low + (rand() * 1.0) * (high - low) / ((long long)(RAND_MAX) + 1.0)));
}
/*
void
operation(int *data, int ind1, int ind2, int value)
{
    if (ind1 != ind2) {
        int tmp1 = data[ind1] - value;
        int tmp2 = data[ind2] + value;

        data[ind1] = tmp1;
        data[ind2] = tmp2;
    }
}
*/
int
main(int argc, char *argv[])
{
    int count, key, nproc, iter_count, *seed;
    sscanf(argv[1], "%d", &count);
    sscanf(argv[2], "%d", &key);
    sscanf(argv[3], "%d", &nproc);
    sscanf(argv[4], "%d", &iter_count);
    seed = calloc(nproc, sizeof(int));    
    for (int i = 0; i < nproc; i++) {
        sscanf(argv[5 + i], "%d", seed + i);
    }
    int shmid = shmget(key, sizeof(int) * count, 0600 | IPC_CREAT);
    int *data = shmat(shmid, 0, 0);
    for (int i = 0; i < count; i++) {
        scanf("%d", data + i);
    }
    int semid = semget(key, count, 0600 | IPC_CREAT);
    struct sembuf cmd = {0, 1, 0};
    for (int i = 0; i < count; i++) {
        cmd.sem_num = i;
        semop(semid, &cmd, 1);
    }

    for (int i = 0; i < nproc; i++) {
        if (!fork()) {
            srand(seed[i]);
            for (int j = 0; j < iter_count; j++) {
                int idx1 = get_rand(0, count);
                int idx2 = get_rand(0, count);
                int value = get_rand(0, 10);
                struct sembuf cur_cmd[2] = {{idx1, -1, 0}, {idx2, -1, 0}};
                semop(semid, cur_cmd, 1 + (idx1 != idx2)); // down
                operation(data, idx1, idx2, value);
                cur_cmd[0].sem_op = cur_cmd[1].sem_op = 1;
                semop(semid, cur_cmd, 1 + (idx1 != idx2)); // up
            }
            return 0;
        }
    }
    while (wait(NULL) > 0) {}
    for (int i = 0; i < count; i++) {
        printf("%d\n", data[i]);
        fflush(stdout);
    }
    semctl(semid, count, IPC_RMID);
    shmctl(shmid, IPC_RMID, 0);
    free(seed);
    return 0;
}

